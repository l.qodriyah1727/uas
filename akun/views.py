from django.shortcuts import render

# Create your views here.
from .models import Post, Sarat, Privasi
def index(request):
    #query set
    posts = Post.objects.all()
    sarats = Sarat.objects.all()
    privasi = Privasi.objects.all()

    context = {
        'Posts' : posts,
        'Sarats' : sarats,
        'Privasi' : privasi,
    }

    return render(request,'modul2/akun.html',context)
