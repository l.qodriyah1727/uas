from django.contrib import admin

# Register your models here.
from . models import Post, Sarat, Privasi
admin.site.register(Post);
admin.site.register(Sarat);
admin.site.register(Privasi);