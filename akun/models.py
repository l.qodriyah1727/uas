from django.db import models

# Create your models here.
class Post(models.Model):
    judul = models.CharField(max_length=500)
    konten = models.TextField()

    def __str__(self):
        return "".format(self.judul)

class Sarat(models.Model):
    judul = models.CharField(max_length=500)
    konten = models.TextField()

    def __str__(self):
        return "".format(self.judul)

class Privasi(models.Model):
    judul = models.CharField(max_length=500)
    konten = models.TextField()

    def __str__(self):
        return "".format(self.judul)