from django.shortcuts import render

# Create your views here.
from .models import Post
def index(request):
    #query set
    posts = Post.objects.all()

    context = {
        'Posts' : posts,
    }

    if request.method == 'POST':
        context['email'] = request.POST['email']
        context['message'] = request.POST['message']

    return render(request,'history.html',context)